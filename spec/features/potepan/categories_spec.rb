require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, name: 'Ruby on Rails', taxonomy: taxonomy) }
  given(:other_taxon) { create(:taxon, name: 'Ruby Bag', taxonomy: taxonomy) }
  given!(:product) { create(:product, name: 'Ruby on Rails Tote', taxons: [taxon]) }
  given!(:other_product) { create(:product, taxons: [other_taxon]) }
  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "show template including" do
    within '.navbar-side-collapse' do
      expect(page).to have_content taxonomy.name
    end

    within '.for-categories-spec' do
      expect(page).to have_content taxon.name
    end

    within '.productCaption' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_no_content other_product.name
    end

    expect(page).to have_title "Ruby on Rails - BIGBAG Store"
  end

  scenario "click to Home-btn" do
    click_on 'Home', match: :first
    expect(page).to have_title "BIGBAG Store"
    within '.class-for-test' do
      expect(page).to have_content "人気カテゴリー"
    end
  end

  scenario "click to link" do
    within '.link-for-product' do
      click_on 'link-for-product-page'
    end
    expect(page).to have_title "Ruby on Rails Tote - BIGBAG Store"
  end
end
