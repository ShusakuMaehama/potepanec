require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:taxon) { create(:taxon, name: 'Ruby on Rails') }
  given(:product) { create(:product, name: 'Ruby on Rails Tote', taxons: [taxon]) }
  given!(:related_product) { create(:product, name: 'Ruby on Rails Mag', taxons: [taxon]) }
  background do
    visit potepan_product_path(product.id)
  end

  scenario "show template including" do
    within ".class-for-product-test" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    within ".related-products" do
      expect(page).to have_content related_product.name
    end
    expect(page).to have_title "Ruby on Rails Tote - BIGBAG Store"
  end

  scenario "click to Home-btn" do
    click_on "Home", match: :first
    within '.class-for-test' do
      expect(page).to have_content "人気カテゴリー"
    end
    expect(page).to have_title "BIGBAG Store"
  end

  scenario "click to 一覧ページへ戻る" do
    click_on "一覧ページへ戻る"
    expect(page).to have_title "Ruby on Rails - BIGBAG Store"
  end

  scenario "click to related_product" do
    click_on "related_product"
    expect(page).to have_title "Ruby on Rails Mag - BIGBAG Store"
  end

  scenario "input to box" do
    fill_in "searchWord", with: "あいうえお"
  end
end
