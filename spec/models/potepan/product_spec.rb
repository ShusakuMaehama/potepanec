require 'rails_helper'

RSpec.feature "Product", type: :model do
  let!(:taxon) { create(:taxon) }
  let!(:other_taxon) { create(:taxon) }
  let!(:product) { create(:product, name: "bag", taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
  let!(:not_related_product) { create(:product, taxons: [other_taxon]) }
  it "match related_products" do
    expect(product.acquisition_related_product).to match related_products
  end
end
