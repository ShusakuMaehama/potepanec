require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET /show' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
    before do
      get :show, params: { id: product.id }
    end

    it "@product has correct value" do
      expect(assigns(:product)).to eq product
    end

    it "@relations has correct value" do
      expect(assigns(:related_products)).to match related_products.first(Potepan::ProductsController::MAX_NUMBER)
    end

    it 'responds successfully' do
      expect(response).to be_success
    end

    it 'returns a 200 response' do
      expect(response).to have_http_status "200"
    end

    it "renders the :show template" do
      expect(response).to render_template :show
    end
  end
end
