require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET /show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    before do
      get :show, params: { id: taxon.id }
    end

    it 'taxon has correct value' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'taxonomy has correct value' do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it 'products has correct value' do
      expect(assigns(:products)).to match_array product
    end

    it 'responds successfully' do
      expect(response).to be_success
    end

    it 'returns a 200 response' do
      expect(response).to have_http_status "200"
    end

    it "renders the :show template" do
      expect(response).to render_template :show
    end
  end
end
