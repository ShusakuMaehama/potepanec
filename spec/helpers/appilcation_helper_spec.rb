require 'rails_helper'

RSpec.describe ApplicationHelper do
  it "full title helper" do
    expect(full_title).to eq "BIGBAG Store"
    expect(full_title("Ruby")).to eq "Ruby - BIGBAG Store"
  end
end
