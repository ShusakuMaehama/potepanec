module Potepan::ProductDecorator
  def acquisition_related_product
    Spree::Product.joins(:taxons).where(
      spree_products_taxons: { taxon_id: taxons }
    ).distinct.where.not(id: id).order("id asc")
  end
  Spree::Product.prepend self
end
