class Potepan::ProductsController < ApplicationController
  MAX_NUMBER = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.acquisition_related_product.includes(master: [:default_price, :images]).limit(MAX_NUMBER)
  end
end
